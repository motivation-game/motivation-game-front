context('Turn', () => {

  it('I should be able to see whos turn it is', () => {
    cy.startGameAs('Fin Kingma')
    cy.get('#turn').contains('Fin Kingma')
  })

  it ('before starting my own turn I see the event card that will affect my turn', () => {
    cy.startGameAs('Fin Kingma')
    cy.get('#eventCard').should('exist')
  })

  it('At the beginning of my turn I can draw an action card or a goal card', () => {
    cy.startGameAs('Fin Kingma')

    cy.get('#eventCard').click()
    cy.get('#drawCardPopup').should('exist');
    cy.get('#drawCardPopup').contains('Goal Card')
    cy.get('#drawCardPopup').contains('Action Card')
  })

  it('After drawing a card I can play an action card', () => {
    cy.startGameAs('Fin Kingma')
    cy.get('#eventCard').click()
    cy.get('#drawActionCard').click();
    cy.get('.action-card').first().click({force: true});
    //best way to check if turn has ended with single player is by checking I am now prompted with a new event
    cy.get('#eventCard').should('exist');
  })

  it('I can also decide to skip and play no action cards', () => {
    cy.startGameAs('Fin Kingma')
    cy.get('#eventCard').click()
    cy.get('#drawActionCard').click();
    cy.get('#hand').contains('skip')
    cy.get('#skip').click({force: true})
    //best way to check if turn has ended with single player is by checking I am now prompted with a new event
    cy.get('#eventCard').should('exist');
  })

  it('When I play an action card, the motivation, blockades, and task enjoyments will be updated', () => {
    let originalLine = '0,240 100,240 '
    cy.startGameAs('Fin Kingma')
    cy.get('#eventCard').click()
    cy.get('#drawActionCard').click();
    cy.get('.action-card').first().click({force: true});
    cy.get('#motivationLine').should('not.have.attr', 'points', originalLine)

  })
  
})
