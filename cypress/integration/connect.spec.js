context('Connect', () => {

  it('I should be able to connect to the game server', () => {
    cy.visit('http://localhost:5000')

    cy.get('#username').type('Fin Kingma')
    cy.get('#submit').click()
    cy.get('#players').contains('Fin Kingma')
  })

  it('The game begins after someone clicks start', () => {
    cy.visit('http://localhost:5000')

    cy.get('#username').type('Fin Kingma')
    cy.get('#submit').click()
    cy.get('#start').click()
    cy.get('#motivationChart').should('exist');
  })
})
