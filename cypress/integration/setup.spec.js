context('Game setup', () => {

  it('When the game begins I get 3 action cards and 2 goal cards', () => {
    cy.startGameAs('Fin Kingma')

    cy.get('.action-card').should('have.length', 3)
    cy.get('.goal-card').should('have.length', 2)
  })
})
