import Vue from "vue";
import Index from "./components/Index.vue";

let v = new Vue({
    el: "#app",
    template: `
        <index />
    `,
    components: {
        Index
    }
});
