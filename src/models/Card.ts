export class Card {
    id:number = 0
    type:string = ''
    title:string = ''
    description:string|undefined = undefined
    blockadeEffect:number|undefined = undefined
    motivationEffect:number|undefined = undefined
    enjoymentEffect:string|undefined = undefined
    completion:string|undefined = undefined
    points:number|undefined = undefined
    completed:boolean = false
}