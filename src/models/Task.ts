export class Task {
    name:string = ''
    enjoyment:number = 0
    color:string = ''
    isActive:boolean=false
    cardEffect:number|undefined = undefined

    constructor(name:string, enjoyment:number, color:string) {
        this.name = name
        this.enjoyment = enjoyment
        this.color = color
    }
}