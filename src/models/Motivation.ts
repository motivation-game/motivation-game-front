import { Task } from './Task';

export class Motivation {
    motivation:Array<number> = new Array<number>();
    blockades:Array<number> = new Array<number>();
    activeTasks:Array<Task|undefined> = new Array<Task|undefined>();
    tasks:Array<Task> = new Array<Task>();

    motivationCardEffect:number|undefined = undefined
    blockadeCardEffect:number|undefined = undefined
}