import { Player } from '../models/Player';
import { SocketService } from './SocketService'
import { CardService } from './CardService';
import { Card } from '../models/Card';
import { MotivationService } from './MotivationService';


export class Engine {
  socketService: SocketService = new SocketService()
  fps: number = 60
  playerName: string = ''
  players: Array<Player> = new Array<Player>()
  playing:boolean=false
  myTurn:boolean=false
  currentPlayersTurn:string = ''
  cardService:CardService = new CardService()
  motivationService:MotivationService = new MotivationService(50, 80)
  showPlayedCard:boolean=false
  showEventCard:boolean=false
  pickCard:boolean=false
  showFinalScore:boolean=false
  recentlyPlayedCard:Card|undefined = undefined
  recentEventCard:Card|undefined = undefined

  constructor() {
    
  }
  login(playerName: string) {
    this.playerName = playerName;

    this.socketService.connect(() => {
      this.socketService.listen(this)
      this.socketService.newPlayer(playerName)
    })
  }
  requestStart() {
    this.socketService.requestStart()
  }
  startGame() {
    this.playing=true
    this.motivationService.setGridForPlayers(this.players.length)
    this.cardService.drawGoalCard()
    this.cardService.drawGoalCard()
    this.cardService.drawActionCard()
    this.cardService.drawActionCard()
    this.cardService.drawActionCard()
  }
  playCard(card: Card) {
    if (this.myTurn) {
      this.motivationService.removeCardEffect()
      this.motivationService.updateMotivationByCard(card);
      this.socketService.playCard(card.id, this.cardService.currentEventCard.id)
      this.cardService.discardCard(card)
    }
  }
  updateMotivationByCard(cardId:number, eventCardId:number) {
    this.recentlyPlayedCard = undefined
    for (let card of this.cardService.cards) {
      if (card.id === cardId) {
        this.motivationService.updateMotivationByCard(card);
        this.recentlyPlayedCard = card
      }

      if (card.id === eventCardId) {
        this.motivationService.updateMotivationByCard(card);
        this.recentEventCard = card
      }
    }
    this.showPlayedCard = true
  }
  skip() {
    if (this.myTurn) {
      this.socketService.playCard(0, this.cardService.currentEventCard.id)
    }
  }
  startTurn(name:string) {
    console.log('turn of ' + name)
    this.checkCompletedGoalCards()
    this.motivationService.nextTurn()
    this.motivationService.removeCardEffect()

    this.currentPlayersTurn = name

    if (name === this.playerName) {
      this.myTurn = true
      this.showEventCard = false
      this.pickCard = false
      this.getEventCard()
    } else {
      this.myTurn = false
      this.showEventCard = false
      this.pickCard = false
    }
  }
  acceptPlayedCard() {
    this.showPlayedCard = false
  }
  getEventCard() {
    this.cardService.drawEventCard()
    this.motivationService.updateMotivationByCard(this.cardService.currentEventCard);
    this.showEventCard = true
  }
  acceptEventCard() {
    this.showEventCard = false
    this.pickCard = true
  }
  drawActionCard() {
    this.cardService.drawActionCard()
    this.pickCard = false
  }
  drawGoalCard() {
    this.cardService.drawGoalCard()
    this.pickCard = false
  }
  checkCompletedGoalCards() {
    for (let card of this.cardService.goalCardsInHand) {
      if (!card.completed && card.completion) {
        if (card.completion.startsWith('blockade')) {
          this.checkBlockadeGoalsCompleted(card)
        } else {
          this.checkTaskGoalsCompleted(card)
        }
      }
    }
  }
  checkTaskGoalsCompleted(card:Card) {
    if (card.completion) {
      let values:string[] = card.completion.split(':')
      let taskName = values[0]
      let requiredTurns:number = +values[1].trim()
      let actualTurns:number = 0
  
      for (let i=0;i<=this.motivationService.turn;i++) {
        let activeTask = this.motivationService.motivation.activeTasks[i]
        if (activeTask && activeTask.name === taskName) {
          actualTurns++
        } else if (activeTask && taskName === 'any') {
          actualTurns++
          continue
        } else {
          actualTurns = 0
        }
      }
      if (actualTurns >= requiredTurns) {
        card.completed = true
      }
    }
  }
  checkBlockadeGoalsCompleted(card:Card) {
    if (card.completion) {
      let values:string[] = card.completion.split(':');
      if (values.length === 2) {
        //simple check blockade is higher than
        let requiredBlockade:number = +values[1].trim()
        for (let i=0;i<this.motivationService.turn;i++) {
          if (this.motivationService.motivation.blockades[i] >= requiredBlockade) {
            card.completed = true
            return
          }
        }
      } else {
        //check if motivated for x turns, with x blockade
        let requiredBlockade:number = +values[1].trim()
        let requiredTurns:number = +values[2].trim()
        let actualTurns:number = 0
        for (let i=0;i<this.motivationService.turn;i++) {
          if (this.motivationService.motivation.activeTasks[i] && this.motivationService.motivation.blockades[i] >= requiredBlockade) {
            actualTurns++
            continue
          } else {
            actualTurns = 0
          }
        }
        if (actualTurns >= requiredTurns) {
          card.completed = true
        }
      }
      
    }
  }
  gameWrapup() {
    this.myTurn = false
    let score = 0
    for (let card of this.cardService.goalCardsInHand) {
      if (card.completed && card.points) {
        score+= card.points
      }
    }
    this.socketService.sendFinalScore(this.playerName, score)
  }
  endGame() {
    this.showFinalScore = true
    this.playing = false
  }
}
