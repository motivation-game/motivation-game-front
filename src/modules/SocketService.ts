const io = require('socket.io-client');
import { Engine } from './Engine'
import { Player } from '../models/Player'

export class SocketService {
  socket:any

  constructor () {
  }

  connect(callback:any) {
    let server = 'http://' + process.env.GAME_SERVER + ':3000/';
    console.log('using server: ' + server)
    this.socket = io(server)

    this.socket.on('connect', () => {
        console.log('connected to socket io gameserver with id: ' + this.socket.id)
        callback()
    })
  }
  listen(engine:Engine) {
    this.socket.on('update-players', (players:Array<Player>) => {
      console.log('new players received')
      engine.players = players;
    })
    
    this.socket.on('start', () => {
      engine.startGame()
    })

    this.socket.on('new-turn', (name:string) => {
      engine.startTurn(name)
    })

    this.socket.on('card-played', (cardId:number, eventCardId:number) => {
      engine.updateMotivationByCard(cardId, eventCardId)
    })

    this.socket.on('game-end', () => {
      engine.gameWrapup()
    })

    this.socket.on('final-scores', (players:Array<Player>) => {
      console.log('all scores have been received, ending game')
      engine.players = players;
      engine.endGame()
    })
  }
  newPlayer(name:string) {
    this.socket.emit("new-player", name);
  }
  disconnect() {
    this.socket.disconnect()
  }
  requestStart() {
    this.socket.emit("start");
  }
  playCard(card:number, eventCard:number) {
    this.socket.emit("play-card", card, eventCard)
  }
  sendFinalScore(player:string, score:number) {
    this.socket.emit("score", player, score)
  }
}