import { Card } from '../models/Card';
import { Motivation } from '../models/Motivation';
import { Task } from '../models/Task';

export class MotivationService {
    motivation: Motivation = new Motivation()
    linesInGrid:number=0
    width:number=996
    height:number=296
    margin:number=2
    turn:number=0

    constructor(blockade:number, motivation: number) {
        this.motivation.blockades[this.turn] = blockade
        this.motivation.motivation[this.turn] = motivation
        this.motivation.tasks.push(new Task('cleaning', -10, 'yellowgreen'))
        this.motivation.tasks.push(new Task('football', 10, 'olive'))
        this.motivation.tasks.push(new Task('drawing', 0, 'turquoise'))
    }

    nextTurn() {
        let oldBlockades = this.motivation.blockades[this.turn]
        let oldMotivation = this.motivation.motivation[this.turn]
        let oldActiveTasks = this.motivation.activeTasks[this.turn]
        this.turn++
        this.motivation.blockades[this.turn] = oldBlockades
        this.motivation.motivation[this.turn] = oldMotivation
        this.motivation.activeTasks[this.turn] = oldActiveTasks

        for (let task of this.motivation.tasks) {
            if (task.isActive) {
                this.motivation.motivation[this.turn] += task.enjoyment
                task.enjoyment += 5
                this.keepMotivationWithinLimits()
            }
        }
    }

    keepMotivationWithinLimits() {
        if (this.motivation.motivation[this.turn] > 100) {
            this.motivation.motivation[this.turn] = 100
        } else if (this.motivation.motivation[this.turn] < 0) {
            this.motivation.motivation[this.turn] = 0
        }

        if (this.motivation.blockades[this.turn] > 100) {
            this.motivation.blockades[this.turn] = 100
        } else if (this.motivation.blockades[this.turn] < 0) {
            this.motivation.blockades[this.turn] = 0
        }
    }

    updateMotivationByCard(card:Card) {
        if (card.blockadeEffect) {
            this.motivation.blockades[this.turn] += card.blockadeEffect

            this.keepMotivationWithinLimits()
        }

        if (card.motivationEffect) {
            this.motivation.motivation[this.turn] += card.motivationEffect
            this.keepMotivationWithinLimits()
        }

        if (card.enjoymentEffect) {
            let values:string[] = card.enjoymentEffect.split(':')
            for (let task of this.motivation.tasks) {
                if (task.name === values[0]) {
                    task.enjoyment += +values[1].trim()

                    if (card.motivationEffect && card.motivationEffect > 0 && this.motivation.motivation[this.turn] > this.motivation.blockades[this.turn]) {
                        for (let task of this.motivation.tasks) {
                            task.isActive = false
                        }
                        task.isActive = true
                        this.motivation.activeTasks[this.turn] = task
                    }
                }
            }
        } else if (card.motivationEffect) {
            // a card with impact on your motivation, without a specific task, will activate the task with the highest enjoyment
            let highestEnjoyment:number = 0
            for (let task of this.motivation.tasks) {
                if (task.enjoyment > highestEnjoyment) {
                    highestEnjoyment = task.enjoyment
                }
            }

            for (let task of this.motivation.tasks) {
                if (task.enjoyment === highestEnjoyment) {
                    task.isActive = true
                    break
                }
            }
        }

        if (this.motivation.motivation[this.turn] <= this.motivation.blockades[this.turn]) {
            for (let task of this.motivation.tasks) {
                task.isActive = false
                this.motivation.activeTasks[this.turn] = undefined
            }
        }
    }
    showCardEffect(card:Card) {
        this.motivation.motivationCardEffect = card.motivationEffect
        this.motivation.blockadeCardEffect = card.blockadeEffect
        if (card.enjoymentEffect) {
            for (let task of this.motivation.tasks) {
                let values:string[] = card.enjoymentEffect.split(':')
                if (values[0] === task.name) {
                    task.cardEffect = +values[1].trim()
                }
            }
        }
    }
    removeCardEffect() {
        this.motivation.motivationCardEffect = undefined
        this.motivation.blockadeCardEffect = undefined
        for (let task of this.motivation.tasks) {
            task.cardEffect = undefined
        }
    }

    setGridForPlayers(amountOfPlayers:number) {
        let rounds=10
        this.linesInGrid = (rounds * amountOfPlayers)
    }
}