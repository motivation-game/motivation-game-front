export class Utils {
    static toRadians (angle:number) {
        return angle * (Math.PI / 180)
    }
    static toDegrees (angle:number) {
        return angle / (Math.PI / 180)
    }
}