import { Card } from '../models/Card';
import goals from '../assets/card-goals.json';
import events from '../assets/card-events.json';
import actions from '../assets/card-actions.json';

export class CardService {
    cards: Array<Card> = new Array<Card>();
    
    eventCardDeck: Array<Card> = new Array<Card>();
    actionCardDeck: Array<Card> = new Array<Card>();
    goalCardDeck: Array<Card> = new Array<Card>();

    actionCardsInHand: Array<Card> = new Array<Card>();
    goalCardsInHand: Array<Card> = new Array<Card>();
    currentEventCard:Card;

    constructor() {
        this.loadCards()
        this.loadCardIdsInDeck();
        this.currentEventCard=this.eventCardDeck[0]
    }

    loadCards() {
        for (let cardData of goals.cards) {
            this.addCardBasedOnData(cardData)
        }
        for (let cardData of events.cards) {
            this.addCardBasedOnData(cardData)
        }
        for (let cardData of actions.cards) {
            this.addCardBasedOnData(cardData)
        }
    }

    addCardBasedOnData(cardData:any) {
        let card: Card = new Card()
        card.id = cardData.id;
        card.type = cardData.type;
        card.title = cardData.title
        card.description = cardData.description
        card.blockadeEffect = cardData.blockadeEffect
        card.motivationEffect = cardData.motivationEffect
        card.enjoymentEffect = cardData.enjoymentEffect
        card.completion = cardData.completion
        card.points = cardData.points

        this.cards.push(card)
    }

    loadCardIdsInDeck() {
        for (let card of this.cards) {
            switch (card.type) {
                case 'event':
                    this.eventCardDeck.push(card)
                    break;
                case 'action':
                    this.actionCardDeck.push(card)
                    break;
                case 'goal':
                    this.goalCardDeck.push(card)
                    break;
            }
        }
    }

    drawGoalCard() {
        let chosenCard = Math.floor(Math.random()*this.goalCardDeck.length)
        this.goalCardsInHand.push(this.goalCardDeck[chosenCard])
        this.goalCardDeck.splice(chosenCard,1)
    }

    drawActionCard() {
        let chosenCard = Math.floor(Math.random()*this.actionCardDeck.length)
        this.actionCardsInHand.push(this.actionCardDeck[chosenCard])
        this.actionCardDeck.splice(chosenCard,1)
    }

    drawEventCard() {
        let chosenCard = Math.floor(Math.random()*this.eventCardDeck.length)
        this.currentEventCard = this.eventCardDeck[chosenCard];
    }

    discardCard(card:Card) {
        if (card.type === 'action') {
            this.actionCardDeck.push(card)
            for (let i in this.actionCardsInHand) {
                if (this.actionCardsInHand[i].id === card.id) {
                    this.actionCardsInHand.splice(+i,1)
                }
            }
        } else {
            this.goalCardDeck.push(card)
            for (let i in this.goalCardsInHand) {
                if (this.goalCardsInHand[i].id === card.id) {
                    this.goalCardsInHand.splice(+i,1)
                }
            }
        }
    }
}