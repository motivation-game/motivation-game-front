configure_aws_cli(){
	aws configure set default.region eu-central-1
	aws configure set default.output json
}
get_game_server(){
    taskFull=$(aws ecs list-tasks --cluster motivation-game-server --query taskArns[0] --output text)
    taskId=${taskFull#*/}
    eni=$(aws ecs describe-tasks --cluster motivation-game-server --tasks $taskId --query 'tasks[0].attachments[0].details[1]'.value --output text)
    GAME_SERVER=$(aws ec2 describe-network-interfaces --network-interface-ids $eni --query 'NetworkInterfaces[0].Association.PublicIp' --output text)
    echo $GAME_SERVER
}

configure_aws_cli
get_game_server